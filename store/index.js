import Vue from 'vue'
import Vuex from 'vuex'
import menu from './modules/menu'
import axios from 'axios';
Vue.use(Vuex)
const createStore = () => {
  return new Vuex.Store({
    modules: {
      menu,
    },

    mutations: {
      initState: (state, payload) => {
        state.menu.data = payload;
      }
    },

    actions: {
      async nuxtServerInit (context) {
        await axios.get(process.env.API_URL + '/menu')
        .then(resp => {
          console.log(process.env.API_URL);
          context.commit('initState', resp.data)
        })
        .catch(error => {
            console.log(error);
        }); 
      }
    }
  })
}
export default createStore
